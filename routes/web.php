<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/generate', function () {
    $parser = new \Nathanmac\Utilities\Parser\Parser();

    $items = $parser->xml(file_get_contents('http://sspbrno.cz/rss.php'))['channel']['item'];
    foreach ($items as &$item) {
        $item['description'] = str_replace('&nbsp;', '', $item['description']);
        $item['pubDate'] = new DateTime($item['pubDate']);

        $url = str_replace('view.php?', 'rservice.php?akce=tisk&', $item['link']);
        $content = iconv('windows-1250', 'UTF-8', file_get_contents($url));
        $content = str_replace(["\r", "\n"], '', $content);

        $item['description'] = (new \DiDom\Document($content))->find('html > body .cla-text')[0]->innerHtml();

        $page = new \App\Pages();
        $page->title = $item['title'];
        $page->content = $item['description'];
        $page->created_at = $item['pubDate'];
        $page->updated_at = $item['pubDate'];
        $page->save();
    }

    //var_dump($items);
});

Route::get('/{any}', 'SinglePageController@index')->where('any', '.*');
