<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['api'])->group(function () {

    // User
    Route::post('/login', 'API\UserController@login');
    Route::get('/logout', 'API\UserController@logout');
    Route::post('/register', 'API\UserController@register');
    Route::get('/user-data', 'API\UserController@userData');
    Route::post('/user/change-password', 'API\UserController@changePassword');
    Route::post('/user/save', 'API\UserController@save');
    Route::get('/user/get', 'API\UserController@getOne');

    // Pages
    Route::get('/page', 'API\PageController@getPage');
    Route::post('/page/save', 'API\PageController@savePage');
    Route::delete('/page/delete', 'API\PageController@deletePage');
    Route::get('/pages', 'API\PageController@getPages');


    Route::get('/component/menu', 'API\MenuController@getMenu');
    Route::post('/component/menu/save', 'API\MenuController@save');
    Route::delete('/component/menu/delete', 'API\MenuController@delete');

    Route::get('/component/menu/add', function ($title, $url) {
        \App\Menu::create([
            'title' => $title,
            'url' => $url
        ]);
    });

    Route::get('/image-upload/token', function () {
        return 'BIzaSyAhKXa628rwVPExnoJW1NDI3rnZ5Uuou40';
    });

    Route::post('/image-upload/upload', function (Request $request) {
        if ($request->hasFile('file')) {
            $image = $request->file('file');
            $fileName = time().'.'.$image->getClientOriginalExtension();
            $img = \Intervention\Image\Facades\Image::make($image->getRealPath());
            $img->stream();

            Storage::disk('local')->put('public/images/1/gallery/'.$fileName, $img, 'public');
            return [
                'uploaded' => 1,
                'fileName' => $image->getClientOriginalName(),
                'url' => URL::to('storage/images/1/gallery/'.$fileName),
                'default' => URL::to('storage/images/1/gallery/'.$fileName),
            ];
        }
        return [
            'uploaded' => 0,
            'error' => [
                'message' => 'Soubor nebylo možné nahrát.'
            ]
        ];
    });
});
