<?php


namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Pages;
use App\User;
use Illuminate\Http\Request;

class PageController extends Controller
{

    public function savePage(Request $request) {
        $user = \Auth::user();
        return \App\Pages::updateOrCreate(['id' => $request->post('id')],[
            'id' => $request->post('id'),
            'title' => $request->post('title'),
            'content' => $request->post('content'),
            'author_id' => $user->id
        ])->get(['id', 'title', 'content']);
    }

    public function getPage(Request $request) {
        $page = \App\Pages::where('id', $request->get('id'))->get(['id', 'title', 'content', 'author_id'])[0];
        if ($page->author_id !== null) {
            $author = User::where('id', $page->author_id)->get()[0];
            $page->author_name = $author->name;
        } else  {
            $page->author_name = null;
        }
        return $page;
    }

    public function deletePage(Request $request) {
        return [
            'deleted' => Pages::where('id', $request->get('id'))->delete()
        ];
    }

    public function getPages(Request $request) {
        return \App\Pages::all();
    }

}