<?php


namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Menu;
use Illuminate\Http\Request;

class MenuController extends Controller
{

    public function save(Request $request) {
        return \App\Menu::create([
            'title' => $request->post('title'),
            'url' => $request->post('url')
        ])->get(['id', 'title', 'url']);
    }

    public function getMenu(Request $request) {
        return \App\Menu::all(['id', 'title', 'url']);
    }

    public function delete(Request $request) {
        return [
            'deleted' => Menu::where('id', $request->get('id'))->delete()
        ];
    }
}