<?php

namespace App\Http\Controllers\API;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;

class UserController extends Controller
{
    public $successStatus = 200;
    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        if(auth()->guard('web')->attempt(['email' => request('email'), 'password' => request('password')])){
            $user = auth()->guard('web')->user();
            $success['token'] =  $user->createToken('MyApp')-> accessToken;
            return response()->json(['success' => $success], $this-> successStatus);
        }
        else{
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }

    public function logout (Request $request) {

        $token = $request->user()->token();
        $token->revoke();

        $response = 'You have been succesfully logged out!';
        return response($response, 200);

    }

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'password' => 'required',
            'passwordRepeat' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('MyApp')-> accessToken;
        $success['name'] =  $user->name;
        return response()->json(['success'=>$success], $this->successStatus);
    }

    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function userData()
    {
        $user = Auth::user();
        if ($user !== null) {
            $user = $user->toArray();
            return response()->json(['success' => $user], $this->successStatus);
        } else {
            return response()->json(['success' => false], $this->successStatus);
        }
    }

    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function getOne(Request $request)
    {
        return User::where('id' , $request->get('id'))->get(['name', 'phone', 'email'])[0];
    }

    public function changePassword(Request $request) {
        if ($request->post('password') == $request->post('passwordRep')) {
            $user = Auth::user();
            $user->password = \Hash::make($request->post('password'));
            $user->save();
        }
    }

    public function save(Request $request) {
        $user = Auth::user();
        $user->name = $request->post('name');
        $user->phone = $request->post('phone');
        $user->save();
    }
}
