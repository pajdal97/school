<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Menu
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu query()
 * @mixin \Eloquent
 */
class Menu extends Model
{
    protected $table = 'menu';

    protected $fillable = [
        'title', 'url'
    ];
}
