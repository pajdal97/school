<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Pages
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pages newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pages newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pages query()
 * @mixin \Eloquent
 */
class Pages extends Model
{
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const EDIT_DATA = 'edit page';

    protected $table = 'pages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'content', 'author_id'
    ];

    public $timestamps = true;

    const SHORT_LEN = 512;

    public static function all($columns = ['*'])
    {
        $return = parent::all($columns);

        foreach ($return as &$item) {
            $item->content = trim(str_replace('&nbsp;', '', $item->content));
            if ($item->author_id !== null) {
                $author = User::where('id', $item->author_id)->get()[0];
                $item->author_name = $author->name;
            } else {
                $item->author_name = null;
            }

            $shortContent = strip_tags($item->content);
            $item->shortContent = strlen($shortContent) > self::SHORT_LEN
                ? (mb_substr($shortContent, 0, self::SHORT_LEN, 'UTF-8') . '...')
                : $shortContent;
        }

        return $return;
    }
}
