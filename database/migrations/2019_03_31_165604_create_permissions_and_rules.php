<?php

use App\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class CreatePermissionsAndRules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permissions = array_flip([
            \App\Pages::EDIT_DATA
        ]);
        foreach ($permissions as $name => &$permission) {
            $permission = Permission::create(['name' => $name]);
        }

        $roles = [
            User::ROLE_STUDENT => [],
            User::ROLE_ADMIN => [
                \App\Pages::EDIT_DATA
            ]
        ];
        foreach ($roles as $name => $rolePermissions) {
            $roleTmp = Role::create(['name' => $name]);
            foreach ($rolePermissions as $rolePermission) {
                $roleTmp->givePermissionTo($permissions[$rolePermission]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
