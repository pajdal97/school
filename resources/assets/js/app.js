
/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */

import './bootstrap';

import Vue from 'vue';
import Vuetify from 'vuetify';
import BootstrapVue from 'bootstrap-vue';
import VueCookie from 'vue-cookie';
import User from './plugins/user';
import CKEditor from '@ckeditor/ckeditor5-vue';

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import Routes from './routes.js';

import App from './views/App';

Vue.use(Vuetify);
Vue.use(BootstrapVue);
Vue.use(VueCookie);
Vue.use(User);
Vue.use(CKEditor);


const app = new Vue({
    el: '#app',
    router: Routes,
    render: h => h(App),
});

export default app;

