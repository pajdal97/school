import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from './pages/Home';
import About from './pages/About';
import Profile from './pages/Profile';
import Page from './pages/Page';
import Menu from './pages/Menu';


Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/about',
            name: 'about',
            component: About
        },
        {
            path: '/profile',
            name: 'profile',
            component: Profile
        },
        {
            path: '/profile/:profileId',
            name: 'profile_show',
            component: Profile
        },
        {
            path: '/page',
            name: 'page',
            component: Page
        },
        {
            path: '/page/:pageId',
            name: 'page_show',
            component: Page
        },
        {
            path: '/menu',
            name: 'menu',
            component: Menu
        }
    ]
});

export default router;
