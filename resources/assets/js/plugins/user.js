import VueCookie from "vue-cookie";
import axios from 'axios';

export default {
    install (Vue, options) {
        Vue.prototype.initUserEnv = () => {
            window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + VueCookie.get('token');
            window.axios.defaults.headers.common['Accept'] = 'application/json';
        };

        Vue.prototype.userData = {
            isAdmin: false
        };
        Vue.prototype.initUserData = (func) => {
            Vue.prototype.initUserEnv();
            axios.get('/api/user-data')
                .then(function (r) {
                    if (r.data.success === false) {
                        Vue.prototype.userData = {
                            isAdmin: false,
                            logged: false
                        };
                        func({
                            isAdmin: false,
                            logged: false
                        });
                    } else {
                        let data = r.data.success;
                        data.logged = true;
                        Vue.prototype.userData = data;
                        func(data);
                    }

                });
        };

    }
}
